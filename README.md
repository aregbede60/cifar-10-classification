# CIFAR 10 CLASSIFICATION

This Project is the classification of images of 10 categories. it is based on the famous cifar 10 image data. A neural network is built from scratch using functional API in Keras Tensor Flow.  We gained an accuracy of 85%